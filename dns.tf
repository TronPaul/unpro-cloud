resource "aws_route53_zone" "teamunpro" {
   name = "teamunpro.com."
}

resource "aws_route53_zone" "internal" {
   name = "aws.teamunpro."
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_eip_association" "nat" {
  instance_id = "${aws_instance.nat.id}"
  allocation_id = "eipalloc-a4f14fc1"
}

resource "aws_eip_association" "vpn" {
  instance_id = "${aws_instance.vpn.id}"
  allocation_id = "eipalloc-bdf092d8"
}

resource "aws_eip_association" "mumble" {
  instance_id = "${aws_instance.mumble.id}"
  allocation_id = "eipalloc-a3f092c6"
}

resource "aws_route53_record" "blog" {
   zone_id = "${aws_route53_zone.teamunpro.zone_id}"
   name = "blog.teamunpro.com"
   type = "CNAME"
   ttl = "5"
   records = ["blog.teamunpro.com.s3-website-us-east-1.amazonaws.com"]
}

resource "aws_route53_record" "jumphost" {
   zone_id = "${aws_route53_zone.teamunpro.zone_id}"
   name = "jumphost.teamunpro.com"
   type = "A"
   ttl = "300"
   records = ["${aws_instance.nat.public_ip}"]
}

resource "aws_route53_record" "vpn" {
   zone_id = "${aws_route53_zone.teamunpro.zone_id}"
   name = "vpn.teamunpro.com"
   type = "A"
   ttl = "300"
   records = ["${aws_eip_association.vpn.public_ip}"]
}

resource "aws_route53_record" "mumble" {
   zone_id = "${aws_route53_zone.teamunpro.zone_id}"
   name = "mumble.teamunpro.com"
   type = "A"
   ttl = "300"
   records = ["${aws_eip_association.mumble.public_ip}"]
}

resource "aws_route53_record" "nat_private" {
   zone_id = "${aws_route53_zone.internal.zone_id}"
   name = "nat.aws.teamunpro"
   type = "CNAME"
   ttl = "5"
   records = ["${aws_instance.nat.private_dns}"]
}

resource "aws_route53_record" "salt_master_private" {
   zone_id = "${aws_route53_zone.internal.zone_id}"
   name = "salt-master.aws.teamunpro"
   type = "CNAME"
   ttl = "5"
   records = ["${aws_instance.salt_master.private_dns}"]
}

resource "aws_route53_record" "mumble_private" {
   zone_id = "${aws_route53_zone.internal.zone_id}"
   name = "mumble.aws.teamunpro"
   type = "CNAME"
   ttl = "5"
   records = ["${aws_instance.mumble.private_dns}"]
}

resource "aws_route53_record" "vpn_private" {
   zone_id = "${aws_route53_zone.internal.zone_id}"
   name = "vpn.aws.teamunpro"
   type = "CNAME"
   ttl = "5"
   records = ["${aws_instance.vpn.private_dns}"]
}
