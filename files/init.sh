#!/bin/bash
name=$(ec2-describe-tags --filter "resource-type=instance" --filter "resource-id=$(curl http://169.254.169.254/latest/meta-data/instance-id)" --filter "key=Name" | cut -f5)
roles=$(ec2-describe-tags --filter "resource-type=instance" --filter "resource-id=$(curl http://169.254.169.254/latest/meta-data/instance-id)" --filter "key=Roles" | cut -f5)
cd /tmp
curl -L https://bootstrap.saltstack.com -o bootstrap-salt.sh
sudo sh bootstrap-salt.sh -X -C -A salt-master.aws.teamunpro -j '{"startup_states": "highstate"}'
if test -f /etc/issue && grep 'Amazon Linux' /etc/issue; then
  export EC2_HOME=/opt/aws/apitools/ec2
  export PATH=$PATH:/opt/aws/bin/
  sudo sh bootstrap-salt.sh -A salt-master.aws.teamunpro git 2016.3
fi
if [ -f /etc/profile.d/aws-apitools-common.sh ]; then
  echo "sourcing aws-apitools-common.sh"
  source /etc/profile.d/aws-apitools-common.sh
fi
if echo $roles | grep -q salt_master; then
  while sudo fuser -s /var/lib/dpkg/lock; do sleep 1; done
  sudo mkdir -p /srv/reactor
  cat <<EOF > /srv/reactor/sync_all.sls
sync_grains:
  cmd.saltutil.sync_all:
    - tgt: {{ data['id'] }}
EOF
  sudo sh bootstrap-salt.sh -A localhost -M -J '{"ext_pillar": [{"git": [{"master https://gitlab.com/TronPaul/unpro-pillar.git": [{"env": "base"}]}]}], "s3.buckets": {"base": ["salt-teamunpro"]}, "fileserver_backend": ["git", "s3fs"], "gitfs_remotes": [{"https://gitlab.com/TronPaul/unpro-salt.git": [{"root": "salt"}]}, "https://github.com/TronPaul/sun-java-formula.git", "https://github.com/TronPaul/sensu-formula.git", "https://github.com/TronPaul/rabbitmq-formula.git", "https://github.com/saltstack-formulas/nfs-formula.git", "https://github.com/TronPaul/openvpn-formula.git", "https://github.com/TronPaul/openvpn-client-formula.git", "https://github.com/TronPaul/bind-formula.git", "https://github.com/saltstack-formulas/ntp-formula.git", "https://github.com/TronPaul/deluge-formula.git", "https://github.com/TronPaul/logstash-formula.git", "https://github.com/TronPaul/logstash_forwarder-formula.git", "https://github.com/TronPaul/mopidy-formula.git"], "auto_accept": true, "gitfs_env_whitelist": ["base"], "reactor": [{"minion_start": ["/srv/reactor/sync_all.sls"]}]}' git 2016.3 # saltstack/salt#37628
  sudo salt '*' saltutil.sync_all
  sudo salt '*' state.highstate
fi
if echo $roles | grep -q aws_nat; then
  sudo iptables -t nat -A POSTROUTING -o eth0 -s 10.0.1.0/16 ! -d 10.0.1.0/16 -j MASQUERADE
fi
sudo service salt-minion restart
sudo salt-call saltutil.sync_all
sudo salt-call state.highstate
