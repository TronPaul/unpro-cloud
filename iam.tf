resource "aws_iam_instance_profile" "default" {
    name = "InstanceDefault"
    roles = ["${aws_iam_role.default.name}"]
}

resource "aws_iam_role" "default" {
    name = "InstanceDefault"
    path = "/"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "default" {
    name = "InstanceDefault"
    role = "${aws_iam_role.default.id}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "tag:getResources",
                "tag:getTagKeys",
                "tag:getTagValues",
                "ec2:DescribeTags",
                "ec2:DescribeInstances"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "salt_master" {
    name = "SaltMasterTerraform"
    roles = ["${aws_iam_role.salt_master.name}"]
}

resource "aws_iam_role" "salt_master" {
    name = "SaltMasterTerraform"
    path = "/"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "salt_master" {
    name = "InstanceDefault"
    role = "${aws_iam_role.salt_master.id}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "tag:getResources",
                "tag:getTagKeys",
                "tag:getTagValues",
                "ec2:DescribeTags",
                "ec2:DescribeInstances"
            ],
            "Resource": "*"
        },
        {
            "Sid": "Stmt1439699569000",
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::salt-teamunpro",
                "arn:aws:s3:::salt-teamunpro/*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "mumble" {
    name = "Mumble"
    roles = ["${aws_iam_role.mumble.name}"]
}

resource "aws_iam_role" "mumble" {
    name = "Mumble"
    path = "/"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "mumble" {
    name = "Mumble"
    role = "${aws_iam_role.mumble.id}"
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1421476583000",
      "Effect": "Allow",
      "Action": [
        "s3:GetObject",
        "s3:PutObject"
      ],
      "Resource": [
        "arn:aws:s3:::teamunpro/mumble/*"
      ]
    },
        {
            "Effect": "Allow",
            "Action": [
                "tag:getResources",
                "tag:getTagKeys",
                "tag:getTagValues",
                "ec2:DescribeTags",
                "ec2:DescribeInstances"
            ],
            "Resource": "*"
        }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "vpn" {
    name = "VPN"
    roles = ["${aws_iam_role.vpn.name}"]
}

resource "aws_iam_role" "vpn" {
    name = "VPN"
    path = "/"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "vpn" {
    name = "VPN"
    role = "${aws_iam_role.vpn.id}"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::teamunpro/vpn_ca/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "tag:getResources",
                "tag:getTagKeys",
                "tag:getTagValues",
                "ec2:DescribeTags",
                "ec2:DescribeInstances"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}
