provider "aws" {
  region = "us-east-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners = ["self"]
  filter {
    name = "name"
    values = ["unpro-ubuntu-16.04-x64-ebs-hvm-*"]
  }
}

data "aws_ami" "nat" {
  most_recent = true
  owners = ["amazon"]
  filter {
    name = "name"
    values = ["amzn-ami-vpc-nat-hvm-*"]
  }
}

data "template_file" "cloud_init" {
  template = "${file("files/cloud-init")}"
}

data "template_file" "init_script" {
  template = "${file("files/init.sh")}"
}

data "template_cloudinit_config" "config" {
  gzip = true
  base64_encode = true

  part {
    content_type = "text/cloud-config"
    content = "${data.template_file.cloud_init.rendered}"
  }

  part {
    content_type = "text/x-shellscript"
    content = "${data.template_file.init_script.rendered}"
  }
}

resource "aws_key_pair" "teamunpro" {
  key_name = "teamunpro" 
  public_key = "${file("teamunpro.pub")}"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  tags {
    Network = "Public"
  }
}

resource "aws_subnet" "public" {
  vpc_id = "${aws_vpc.main.id}"
  cidr_block = "10.0.0.0/24"
  availability_zone = "us-east-1c"
}

resource "aws_subnet" "private" {
  vpc_id = "${aws_vpc.main.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1c"
}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"
  tags {
    Name = "main"
  }
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.main.id}"
  }
  route {
    cidr_block = "172.20.254.0/24"
    instance_id = "${aws_instance.vpn.id}"
  }

  tags {
    Name = "Public"
  }
}

resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    instance_id = "${aws_instance.nat.id}"
  }
  route {
    cidr_block = "172.20.254.0/24"
    instance_id = "${aws_instance.vpn.id}"
  }

  tags {
    Name = "Private"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id = "${aws_subnet.public.id}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "private" {
  subnet_id = "${aws_subnet.private.id}"
  route_table_id = "${aws_route_table.private.id}"
}

resource "aws_default_security_group" "default" {
  vpc_id = "${aws_vpc.main.id}"

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["172.20.254.0/24"]
  }

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "public_ssh" {
  vpc_id = "${aws_vpc.main.id}"
  name = "PublicSSH"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "public_icmp" {
  vpc_id = "${aws_vpc.main.id}"
  name = "PublicICMP"
  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "mumble" {
  vpc_id = "${aws_vpc.main.id}"
  name = "Mumble"
  ingress {
    from_port = 64738
    to_port = 64738
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 64738
    to_port = 64738
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "vpn" {
  vpc_id = "${aws_vpc.main.id}"
  name = "VPN"
  ingress {
    from_port = 1194
    to_port = 1194
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "nat" {
  ami = "${data.aws_ami.nat.id}"
  instance_type = "t2.nano"
  key_name = "${aws_key_pair.teamunpro.key_name}"
  subnet_id = "${aws_subnet.public.id}"
  source_dest_check = false
  vpc_security_group_ids = ["${aws_default_security_group.default.id}", "${aws_security_group.public_ssh.id}", "${aws_security_group.public_icmp.id}"]
  iam_instance_profile = "${aws_iam_instance_profile.default.id}"
  associate_public_ip_address = true
  user_data = "${data.template_cloudinit_config.config.rendered}"
  tags {
    Name = "nat"
    Roles = "aws_nat"
  }
}

resource "aws_instance" "salt_master" {
  depends_on = ["aws_instance.nat"]
  ami = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.teamunpro.key_name}"
  subnet_id = "${aws_subnet.private.id}"
  iam_instance_profile = "${aws_iam_instance_profile.salt_master.id}"
  vpc_security_group_ids = ["${aws_default_security_group.default.id}"]
  user_data = "${data.template_cloudinit_config.config.rendered}"
  tags {
    Name = "salt-master"
    Roles = "salt_master"
  }
}

resource "aws_instance" "vpn" {
  depends_on = ["aws_route53_record.salt_master_private"]
  ami = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.nano"
  key_name = "${aws_key_pair.teamunpro.key_name}"
  subnet_id = "${aws_subnet.public.id}"
  source_dest_check = false
  vpc_security_group_ids = ["${aws_default_security_group.default.id}", "${aws_security_group.public_icmp.id}", "${aws_security_group.vpn.id}"]
  iam_instance_profile = "${aws_iam_instance_profile.vpn.id}"
  associate_public_ip_address = true
  user_data = "${data.template_cloudinit_config.config.rendered}"
  tags {
    Name = "vpn"
    Roles = "vpn_server"
  }
}

resource "aws_instance" "mumble" {
  depends_on = ["aws_route53_record.salt_master_private"]
  ami = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.nano"
  key_name = "${aws_key_pair.teamunpro.key_name}"
  subnet_id = "${aws_subnet.public.id}"
  vpc_security_group_ids = ["${aws_default_security_group.default.id}", "${aws_security_group.public_icmp.id}", "${aws_security_group.mumble.id}"]
  iam_instance_profile = "${aws_iam_instance_profile.mumble.id}"
  associate_public_ip_address = true
  user_data = "${data.template_cloudinit_config.config.rendered}"
  tags {
    Name = "mumble"
    Roles = "voice_server"
  }
}
